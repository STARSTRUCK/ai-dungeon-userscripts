// =========================================
//  NOTE FOR DEVELOPERS:
//  This file was generated from the
//  folder artdungeon.
//  If you intend to push changes, do not
//  modify this file directly or they will
//  be lost on recompile.
// =========================================
//  artdungeon/UserConfig.first.js
// =========================================
// ==UserScript==
// @name         Art Dungeon
// @namespace    https://gitlab.com/STARSTRUCK/ai-dungeon-userscripts
// @version      0.2.0
// @description  Embed Artbreeder images into AI Dungeon adventures and scenarios
// @author       STARSTRUCK / @corolla_johnson
// @match        https://play.aidungeon.io/*
// @require      http://code.jquery.com/jquery-latest.js
// ==/UserScript==

// +---------------------------------------+
// | User Config - Change these as needed! |
// +---------------------------------------+
const USER_CONFIG = {
    // If true, will wait for text to finish changing
    // before adding images. Reduces jerking/flashing.
    slowScheduler: false,

    // Whether to use wide name tags.
    wideNameTags: true,
    
    // Whether images should appear to the left of text.
    // Floating has certain rules that are written below.
    floatLeft: true,

    // The height of small images in line-heights.
    smallImageScale: 5,

    // The height of large images in line-heights.
    largeImageScale: 8,

    // Fullsize images are always 512px tall!

};

/**
 * Examples of Art Dungeon tags:
 *
 * [ard -id a161fcf77098472ed213]
 * - Tag will be replaced by the specified image followed by a newline.
 *
 * [ard -id a161fcf77098472ed213 -scale small -names "John Doe" "Johnny"]
 * - Associates the specified image with the names "John Doe" and "Johnny" (case insensitive)
 *   followed by a colon (i.e. "JOHN DOE:")
 * - Sets a small scale.
 * - Acts on all text BELOW the tag. The association can be overwritten if the same name(s)
 *   are used with a different image later.
 *
 * Full arg list:
 * -id:             The image ID. Required.
 *
 * -scale:          small - (Default) A small size, suitable for automatic dialog portraits
 *                  large - A larger size, suitable for manually-embedded items.
 *                  fullsize  - A very large size suitable for landscapes.
 *                  Exact scales are user-configurable.
 *                  Scales the image. The Artbreeder component will suggest a scale automatically.
 *                  Image will be downloaded at full scale regardless of what you choose.
 *
 * -names:          Must be the last arg. A list of names to associate with the image.
 *                  If not present, the tag will embed the specified image.
 *                  If a tag with -names is successfully parsed, Art Dungeon will replace the tag
 *                  with a message indicating the success.
 * 
 */

/**
 * Floating behaviour:
 *
 * - If USER_CONFIG.floatLeft is true, text will appear to the right of images, saving vertical
 *   space and generally looking quite nice.
 * 
 * - Text will STOP floating after a certain number of blank lines.
 * 
 * - For character portraits, one blank line is enough, e.g.
 *   +---------------------------------------------------------------------+
 *   | JOHN: I've made a huge mess.                                        |
 *   |                                                                     |
 *   | John recalled the series of mistakes that led him to this juncture. |
 *   +---------------------------------------------------------------------+
 *   This lets you intuitively separate dialog from prose.
 * 
 * - For regular images, two blank lines are needed.
 *   +---------------------------------------------------------------------+
 *   | [ard -id a161fcf77098472ed213]                                      |
 *   | John recalled the series of mistakes that led him to this juncture. |
 *   |                                                                     |
 *   |                                                                     |
 *   | In an different town, Jane was gloating about her good luck.        |
 *   +---------------------------------------------------------------------+
 *   Several paragraphs separated by a single blank line wiil float next 
 *   to the same image, but you can still break the floating with two blank lines.
 * 
 */

// end of artdungeon/UserConfig.first.js

// =========================================
//  artdungeon/AdventureFormatter.js
// =========================================
/**
 * 
 */
class AdventureFormatter {
    constructor() {
        // Constants
        this.WATCHDOG_TIMER = 500;
        
        // Properties
        this.nameTags = {};
        this.newlineCount = 0;
        this.maxNewlines = Infinity;
    }

    /**
     * Processes an action node.
     * @param   {string} element - Node to process
     */
    processActionNode(element) {
        console.log(`NewlineCount = ${this.newlineCount}`);

        // Mine text properties while we're at it.
        Components.textColor  = $(element).css("color");
        Components.textHeight = $(element).css("line-height").slice(0, -2);

        // Get HTML for THIS action.
        var html_done = "";
        var html_left = $(element).html();
        console.log(`Original action.html():\n${html_left}`);

        // Do this until we can't find any more names or ard tags.
        for (var loop = 0; loop <= this.WATCHDOG_TIMER; loop++) {

            console.log(`NewlineCount = ${this.newlineCount}`);
            console.log(`MaxNewlines = ${this.maxNewlines}`);

            // Get first point of interest but don't process it yet.
            const poi = this._getFirstPointOfInterestFrom(html_left);

            // If we can't find any more points of interest, abort.
            if (poi == null) {
                console.log("No more points of interest");
                break;
            }

            try {
                var res = poi.process(html_left);

            } catch(err) {
                console.error(`Failed to process point of interest ${poi.text}
                            at ${poi.idx}:\n${err}`);

                var res = poi.ignore(html_left);
            }

            console.log(`Results:`);
            console.log(`res.idx:${res.idx}`);
            console.log(`res.hmtl:\n${res.html}`);
            console.log(`html_done:\n${html_done}`);
            console.log(`html_left:\n${html_left}`);

            html_left = res.html;

            // Split string into processed and pending parts
            html_done = html_done.concat(html_left.slice(0,res.idx));
            html_left = html_left.slice(res.idx);

            if (loop >= this.WATCHDOG_TIMER) {
                console.error(
                    `WDT triggered while processing a node;
                    either ${this.WATCHDOG_TIMER} tags parsed in one node or something went wrong!`);
                break;
            }
        }

        // No more points of interest; concat remainder.
        html_done = html_done.concat(html_left);

        // Finally, commit changes to action span
        $(element).html(html_done);
        console.log(`Committed changes to node:\n${html_done}`);
    }
    
    _getFirstPointOfInterestFrom(html) {
        var pois = new Array();
    
        pois.push(new RawArdTag(this).from(html));
        pois.push(new NameTag(this).from(html));
        pois.push(new OldNameTag(this).from(html));
        pois.push(new OldEmbed(this).from(html));

        if (USER_CONFIG.floatLeft && this.maxNewlines != Infinity) {
            pois.push(new Newline(this).from(html));

            if (this.newlineCount > 0) {
                pois.push(new NotNewline(this).from(html));
            }
        }

        pois.sort((a,b)=>a.idx - b.idx);

        if (!pois[0].exists) {
            return null;
        } else {
            return pois[0];
        }
    }

    resetNewline() {
        this.newlineCount = 0;
    }

    reportPortrait() {
        this.newlineCount = 0;
        this.maxNewlines = 2;
    }

    reportNormalImage() {
        this.newlineCount = 0;
        this.maxNewlines = 3;
    }

    reportNewline() {
        this.newlineCount++;
    }

    reportFloatBreak() {
        this.newlineCount = 0;
        this.maxNewlines = Infinity;
    }

    shouldStopFloating() {
        return this.newlineCount >= this.maxNewlines;
    }
}

// end of artdungeon/AdventureFormatter.js

// =========================================
//  artdungeon/ArdTag.js
// =========================================
/**
 * Parsed Art Dungeon tag
 * @param   {string} tag    - The text of the tag.
 * @returns {ArdTag}        - Parsed ardtag object.
 */
class ArdTag {
    constructor(tag) {
        // +-----------+
        // | Constants |
        // +-----------+
        // Matches the spaces used as delimiters within the tags
        // (unless in quotes; this allows you to specify character names with spaces in them.)
        this.REGEXP_SPLIT_BY_SPACE_EXCEPT_IN_QUOTES = /(?:[^\s"]+|"[^"]*")+/g;

        // Used for input validation.
        // Matches anything other than alphanumeric, underscores, spaces and dashes.
        this.REGEXP_INPUT_SANITY = /[^a-zA-Z0-9_ \-]/m;

        // TT for state_getNextOption
        this.GNO_TRANSITION_TABLE = {
            "ard"   : this._state_getNextOption,
            "-id"   : this._state_getIdArg,
            "-scale": this._state_getScaleArg,
            "-names": this._state_getNamesArg,
            "-noind": this._state_noind,
        };

        // +-----------+
        // | Variables |
        // +-----------+
        this.id = -1;
        this.scale = "small";
        this.names = new Array();
        this.indicate = true;

        console.log(`Parsing tag: ${tag}`);

        // Strip square brackets
        tag = tag.slice(1, -1);

        // Split by spaces
        var tokens = tag.match(this.REGEXP_SPLIT_BY_SPACE_EXCEPT_IN_QUOTES);
        console.log(`Got tokens ${tokens}`);

        // Parse with FSM
        var state = this._state_getNextOption.bind(this);
        for (var currentToken of tokens) {
            
            // Strip quotes from quoted strings
            currentToken = currentToken.replace('\"', "");
            currentToken = currentToken.replace('\"', "");

            // Check for possible HTML injection
            if (currentToken.search(this.REGEXP_INPUT_SANITY) != -1) {
                throw `Illegal characters found in token ${currentToken}`;
            }

            state = state(currentToken).bind(this);
        }

        if (this.id == -1) {
            throw "Tag has no image id";
        }
    }
    
    // +------------+
    // | FSM states |
    // +------------+
    _state_getNextOption(token) {
        console.log(`_state_getNextOption(${token})`);

        var next = this.GNO_TRANSITION_TABLE[token];
        if (next == undefined) {
            throw `Invalid option: ${token}; expected -id, -scale, -noind or -names`;
        }

        return next.bind(this);
    }

    _state_noind(token) {
        console.log(`_state_noind(${token})`);

        this.indicate = false;

        return this._state_getNextOption(token).bind(this);
    }

    _state_getIdArg(token) {
        console.log(`_state_getIdArg(${token})`);

        if (this.GNO_TRANSITION_TABLE[token] != undefined || token[0] == '-') {
            throw `Unexpected option: ${token}; expected image id`;
        }

        this.id = token;
        return this._state_getNextOption.bind(this);
    }

    _state_getScaleArg(token) {
        console.log(`_state_getScaleArg(${token})`);

        if (this.GNO_TRANSITION_TABLE[token] != undefined || token[0] == '-') {
            throw `Unexpected option: ${token}; expected scale`;
        }

        if (Components.imageHeights[token] == undefined) {
            throw `Unknown scale: ${token}; expected small, large or fullsize `;
        }

        this.scale = token;
        return this._state_getNextOption.bind(this);
    }

    _state_getNamesArg(token) {
        console.log(`_state_getNamesArg(${token})`);

        if (this.GNO_TRANSITION_TABLE[token] != undefined || token[0] == '-') {
            throw `Unexpected option: ${token}; expected name`;
        }

        this.names.push(token);
        return this._state_getNamesArg.bind(this);
    }
}


// end of artdungeon/ArdTag.js

// =========================================
//  artdungeon/Components.js
// =========================================
class Components
{
    // Text height in px.
    static textHeight = 10;

    // Text color as rgb(r,g,b).
    static textColor = 'rgb(200, 200, 200)';

    // Scale settings to be set by user.
    static imageHeights = {
        small     : () => {return this.textHeight * USER_CONFIG.smallImageScale;},
        large     : () => {return this.textHeight * USER_CONFIG.largeImageScale;},
        fullsize  : () => {return 512},
    };
    
    static Portrait(ardtag, name) {
        // Add a linebreak to space the portrait from previous text.
        var tag = `<div style="clear:left"><br></div><span style="display:none;">[OLD_NAME_TAG]</span>`;

        if (USER_CONFIG.wideNameTags) {
            tag = tag.concat(this.ImageTag(ardtag, true));

            // Write the name in inverted text and put some color around it.
            tag = tag.concat(`<div style="background-color:${this.textColor};color:Black;`);
            tag = tag.concat(`padding:${this.textHeight/4}px ${this.textColor};">`);

            // Put a space if wide name tags are used without floatLeft
            if (!USER_CONFIG.floatLeft) {
                tag = tag.concat(' ');
            }

            tag = tag.concat(name);
            tag = tag.concat(`</div>`);

        } else {
            tag = tag.concat(this.ImageTag(ardtag, true));

            // Need a linebreak on non float-left configs
            if (!USER_CONFIG.floatLeft) {
                tag = tag.concat('<br>');
            }

            // Write the name in inverted text and put some color around it.
            tag = tag.concat(`<span style="background-color:${this.textColor};color:Black;`);
            tag = tag.concat(`padding:${this.textHeight/4}px;">`);
            tag = tag.concat(name);
            tag = tag.concat(`</span>`);

            // Need a line break if using floatLeft; else we need an enforced space
            if (USER_CONFIG.floatLeft) {
                tag = tag.concat('<br>');
            } else {
                tag = tag.concat(' ');
            }
        }

        return tag;
    }

    static NamelessImage(ardtag) {
        // Add a linebreak to space the portrait from previous text.
        var tag = `<div style="clear:left"><br></div><span style="display:none;">[OLD_EMBED]</span>`;

        tag = tag.concat(this.ImageTag(ardtag));

        // Need at least one line break if not using float left.
        if (!USER_CONFIG.floatLeft) {
            tag = tag.concat('<br>');
        }

        return tag;
    }

    /**
     * Returns a plain HTML image tag from an ArdTag.
     * @param   {ArdTag}  ardtag   - Parsed ardtag object.
     * @returns {string}           - HTML image tag.
     */
    static ImageTag(ardtag, border = false) {
        
        var floatLeft = (ardtag.scale != this.imageHeights.fullsize && USER_CONFIG.floatLeft);

        var src =     `src="https://artbreeder.b-cdn.net/imgs/${ardtag.id}.jpeg"`
        var altText = `alt="Art Dungeon v0.1 - id:${ardtag.id}, scale:${ardtag.scale}, names:${ardtag.names}"`
        var height =  `height=${(this.imageHeights[ardtag.scale])()}px`;

        var style_start  = `style="`;
        var style_float  = floatLeft ? `float:left;clear:left;` : ``;
        var style_border = border ? `border:${this.textHeight/8}px solid ${this.textColor};` : ``;
        var style_margin = `margin-right:${this.textHeight/2}px;`
        var style_end    = `"`;

        var tag =        `<img ${src} ${altText} ${height} ${style_start} ${style_float}`
        tag = tag.concat(`${style_margin} ${style_border} ${style_end}>`)

        return tag;
    }
}


// end of artdungeon/Components.js

// =========================================
//  artdungeon/FastArdScheduler.js
// =========================================
/**
 * The AI Dungeon component of Art Dungeon.
 * Adds images to the adventure.
 */
class FastArdScheduler {
    constructor() {
        // Constants
        this.VERSION            = "0.2.0";
        this.MIN_LOOP_DELAY     = 200;
        this.ACTION_OUTER_CLASS = '#root';
        this.ACTION_NODE_CLASS  = '.r-1xnzce8';

        // Members
        this.actionObserver = null;
        this.loopReady      = false;
        this.dirtyPage      = false;
    }

    start() {
        console.log(`ART DUNGEON ${this.VERSION}: AI Dungeon component`);

        $(document).ready(this._startLoop.bind(this));
    }

    _startLoop() {
        this.loopReady = false;

        this._processAdventure();
    
        this._waitForDomChanges();
    
        setTimeout((()=>{
            if (this.dirtyPage) {
                this.dirtyPage = false;
                this._startLoop();
            } else {
                this.loopReady = true;
            }
        }).bind(this), this.MIN_LOOP_DELAY);
    }

    _processAdventure() {
        console.log(`Processing adventure`);

        // Start fresh each time the adventure is processed
        var formatter = new AdventureFormatter();
    
        // Process all nodes
        var actionNodes = $(this.ACTION_NODE_CLASS);
        squelch(()=>{
            actionNodes.each((index, element)=>{
                try {
                    console.log(`Processing node ${index}`);
                    formatter.processActionNode(element);
                } catch(err) {
                    console.error(`Error while processing node ${index}:\n${err}`);
                }
            });
        });
    
        console.log(`Finished processing ${actionNodes.length} actions`);
    }

    _waitForDomChanges() {
        this.actionObserver = new MutationObserver(this._handleDomChange.bind(this));
    
        var actionParents = $(this.ACTION_OUTER_CLASS);
        actionParents.each ((index, element) => {
            this.actionObserver.observe(element, {
                childList: true,
                characterData: true,
                attributes: true,
                subtree: true});
            });
    
        console.log(`Observing ${actionParents.length} nodes`);
    }

    _handleDomChange(mutationRecord = null) {
        // We only need to detect this once
        this.actionObserver.disconnect();
    
        if (this.loopReady) {
            console.log("_handleDomChange triggered with loop ready--reprocessing");
            this._startLoop();
        } else {
            console.log("_handleDomChange triggered with loop not ready-will reprocess");
            this.dirtyPage = true;
        }
    }
}

// end of artdungeon/FastArdScheduler.js

// =========================================
//  artdungeon/NameTag.js
// =========================================
/**
 * Defines location and text of a recognized name.
 */
class NameTag {
    constructor(formatter) {
        // Properties
        this.idx    = Infinity;
        this.exists = false;
        this.text   = ""
        this.formatter = formatter;
    }

    from(html) {
        var firstIdx = Infinity;
        var firstName = null;

        for (const name in this.formatter.nameTags) {
            const regexp_speechTag = new RegExp(`${name}: `, 'i');

            const idx = html.search(regexp_speechTag);

            if (idx == -1) {
                continue;
            }

            if (idx < firstIdx) {
                firstIdx = idx;
                firstName = name;
            }
        }

        if (firstIdx == Infinity) {
            this.idx = Infinity;
            this.exists = false;
            return;

        } else {
            this.idx = firstIdx;
            this.name = firstName;
            this.exists = true;
        }

        console.log(`Final index for ${this.name} = ${this.idx}`);

        return this;
    }

    process(html) {
        const idx = this.idx;
        const name = this.name;

        console.log(`Adding character image for ${name} at ${idx}`);

        var portrait = Components.Portrait(this.formatter.nameTags[name], name);

        html = replaceAt(html, idx, name.concat(": "), portrait);
        console.log(`New HTML:\n${html}`);

        this.formatter.reportPortrait();
            
        var lastIdx = idx + portrait.length;
        
        return { idx: lastIdx, html: html };
    }

    ignore(html) {
        return { idx: this.idx + this.text.length, html: html };
    }
}

// end of artdungeon/NameTag.js

// =========================================
//  artdungeon/Newline.js
// =========================================
/**
 * Defines location and text of a newline.
 */
class Newline {
    constructor(formatter) {
        this.REGEXP_NEWLINE = /\n/m;

        // Properties
        this.idx    = Infinity;
        this.exists = false;
        this.text   = ""
        this.formatter = formatter;
    }

    from(html) {
        var idx = html.search(this.REGEXP_NEWLINE);

        if (idx == -1) {
            this.idx = Infinity;
            this.exists = false;
            return;

        } else {
            this.idx = idx;
            this.exists = true;
            this.text = '\n';
        }

        return this;
    }

    process(html) {
        const idx = this.idx;
        const text = this.text;

        console.log(`Processing linebreak at ${idx}`);

        this.formatter.reportNewline();

        if (this.formatter.shouldStopFloating()) {
            const specialdiv = `<div style="clear:left"><br></div>`;

            html = replaceAt(html, idx, '\n', specialdiv);

            console.log(`New HTML:\n${html}`);
    
            this.formatter.reportFloatBreak();

            return { idx: idx + specialdiv.length, html: html };
        } else {
            return this.ignore(html);
        }
    }

    ignore(html) {
        return { idx: this.idx + this.text.length, html: html };
    }
}


// end of artdungeon/Newline.js

// =========================================
//  artdungeon/NotNewline.js
// =========================================
/**
 * Defines location and text of a character other than newline.
 */
class NotNewline {
    constructor(formatter) {
        this.REGEXP_NOT_NEWLINE = /[^\n]/m;

        // Properties
        this.idx    = Infinity;
        this.exists = false;
        this.text   = ""
        this.formatter = formatter;
    }

    from(html) {
        var idx = html.search(this.REGEXP_NOT_NEWLINE);

        if (idx == -1) {
            this.idx = Infinity;
            this.exists = false;
            return;

        } else {
            this.idx = idx;
            this.exists = true;
            this.text = html.charAt(idx);
        }

        return this;
    }

    process(html) {
        const idx = this.idx;
        const text = this.text;

        console.log(`Processing non-newline at ${idx}: ${text}`);

        this.formatter.resetNewline();

        return { idx: idx, html: html };
    }

    ignore(html) {
        return { idx: idx, html: html };
    }
}


// end of artdungeon/NotNewline.js

// =========================================
//  artdungeon/OldEmbed.js
// =========================================
/**
 * Defines location and text of an already processed standard image.
 */
class OldEmbed {
    constructor(formatter) {
        this.REGEX_OLD_EMBED = /\[OLD_EMBED\]/m;

        // Properties
        this.idx    = Infinity;
        this.exists = false;
        this.text   = ""
        this.formatter = formatter;
    }

    from(html) {
        var idx = html.search(this.REGEX_OLD_EMBED);

        if (idx == -1) {
            this.idx = Infinity;
            this.exists = false;
            return;

        } else {
            this.idx = idx;
            this.exists = true;
            this.text = '\n';
        }

        return this;
    }

    process(html) {
        const idx = this.idx;
        const text = this.text;

        console.log(`Processing old embed at ${idx}`);

        this.formatter.reportNormalImage();

        return { idx: idx + this.text.length, html: html };
    }

    ignore(html) {
        return { idx: this.idx + this.text.length, html: html };
    }
}


// end of artdungeon/OldEmbed.js

// =========================================
//  artdungeon/OldNameTag.js
// =========================================
/**
 * Defines location and text of an already-processed name.
 */
class OldNameTag {
    constructor(formatter) {
        this.REGEXP_OLD_NAME_TAG = /\[OLD_NAME_TAG\]/m;

        // Properties
        this.idx    = Infinity;
        this.exists = false;
        this.text   = ""
        this.formatter = formatter;
    }

    from(html) {
        var idx = html.search(this.REGEXP_OLD_NAME_TAG);

        if (idx == -1) {
            this.idx = Infinity;
            this.exists = false;
            return;

        } else {
            this.idx = idx;
            this.exists = true;
            this.text = '\n';
        }

        return this;
    }

    process(html) {
        const idx = this.idx;
        const text = this.text;

        console.log(`Processing old name tag at ${idx}`);

        this.formatter.reportPortrait();

        return { idx: idx + this.text.length, html: html };
    }

    ignore(html) {
        return { idx: this.idx + this.text.length, html: html };
    }
}


// end of artdungeon/OldNameTag.js

// =========================================
//  artdungeon/RawArdTag.js
// =========================================
/**
 * Defines location and text of a raw [ard] tag.
 */
class RawArdTag {
    constructor(formatter) {
        // Constants
        this.REGEXP_ARDTAG = /(\[ard)(\w|\d|-| |")*(\])/m;

        // Properties
        this.idx    = Infinity;
        this.exists = false;
        this.text   = ""
        this.formatter = formatter;
    }

    from(html) {
        var idx = html.search(this.REGEXP_ARDTAG);

        if (idx == -1) {
            return this;

        } else {
            this.idx    = idx;
            this.exists = true;
            this.text   = (html.match(this.REGEXP_ARDTAG))[0];
        }
        
        return this;
    }

    process(html) {
        const text = this.text;
        const idx = this.idx;

        try {
            var parsedTag = new ArdTag(this.text);
            console.log(`Successfully parsed tag: ${parsedTag} at ${idx}`);

        } catch (err) {
            console.error(`Ignoring bad tag: ${err} at ${idx}`);
            return { idx: idx + text.length, html: html };
        }

        if (parsedTag.names.length == 0) {
            console.log(`No names found in tag; embedding in place`);

            var image = Components.NamelessImage(parsedTag);

            html = replaceAt(html, idx, text, image);
            var lastIdx = idx + image.length;

            if (USER_CONFIG.floatLeft) {
                // Destroy newline after image if it is present.
                if (html.charAt(lastIdx) == '\n') {
                    console.log("Eating newline");
                    html = replaceAt(html, lastIdx, '\n', '');
                }
            }

            this.formatter.reportNormalImage();

            console.log(`New HTML:\n${html}`);

            return { idx: lastIdx, html: html };

        } else {
            // Generate (or overwrite) a name-tag association.
            for (const name of parsedTag.names) {
                console.log(`New association: ${name}-${parsedTag.id}`);
                this.formatter.nameTags[name] = parsedTag;
            }

            if (parsedTag.indicate) {
                // Add -noind flag to tag (re-link but don't re-indicate)
                const text_noind = "[ard -noind".concat(text.slice(4));

                // Indicate succesful parsing
                // (Leave raw tag for re-linking)
                const indicatedTag = `<nobr><span style="background-color:${Components.textColor};color:black;">
                                &check; Image ${parsedTag.id} linked for names: [${parsedTag.names}]
                                </span></nobr><span style="display:none;">${text_noind}</span>`;

                html = replaceAt(html, idx, text, indicatedTag);
                console.log(`New HTML:\n${html}`);

                this.formatter.resetNewline();

                return { idx: idx + indicatedTag.length, html: html };

            } else {

                this.formatter.resetNewline();

                return { idx: idx + text.length, html: html };
            }
        }
    }

    ignore(html) {
        return { idx: this.idx + this.text.length, html: html };
    }
}

// end of artdungeon/RawArdTag.js

// =========================================
//  artdungeon/SlowArdScheduler.js
// =========================================
/**
 * The AI Dungeon component of Art Dungeon.
 * This class controls the scheduling of AdventureFormatter.
 */
class SlowArdScheduler {
    constructor() {
        // Constants
        this.VERSION            = "0.2.0";
        this.STABILIZE_TIME     = 10;
        this.ACTION_OUTER_CLASS = '#root';
        this.ACTION_NODE_CLASS  = '.r-1xnzce8';

        // Members
        this.domObserver         = null;
        this.dirtyPage           = false;
        this.stabilizing         = false;
    }

    start() {
        console.log(`ART DUNGEON ${this.VERSION}: AI Dungeon component`);

        $(document).ready(this._run.bind(this));
    }

    _run() {
        if (this.domObserver != null) {
            this.domObserver.disconnect();
        }

        this._processAdventure();

        this._setupDomObserver();
    }

    _setupDomObserver() {
        this.domObserver = new MutationObserver(this._handleDomChange.bind(this));
    
        var actionParents = $(this.ACTION_OUTER_CLASS);
        actionParents.each ((index, element) => {
            this.domObserver.observe(element, {
                childList: true,
                characterData: true,
                attributes: true,
                subtree: true});
            });
    
        console.log(`Observing ${actionParents.length} nodes`);
    }

    _handleDomChange(mutationRecord = null) {
        this.dirtyPage = true;

        if (!this.stabilizing) {
            console.log("_handleDomChange triggered while not stabilizing");
            this._waitUntilDomStabilizes();
        }
    }

    _waitUntilDomStabilizes() {
        this.stabilizing = true;

        console.log("Waiting until dom stabilizes...")

        if (!this.dirtyPage) {
            this.dirtyPage = false;
            this.stabilizing = false;
            this._run();
            return;

        } else {
            console.log("Page was dirty")
            this.dirtyPage = false;
            setTimeout(this._waitUntilDomStabilizes.bind(this), 
                       this.STABILIZE_TIME);    
        }
    }

    _waitForDomChange() {
        console.log("waitingForDomChange");
        this.stabilizing = true;
    }

    
    _processAdventure() {
        console.log(`Processing adventure`);

        // Start fresh each time the adventure is processed
        var formatter = new AdventureFormatter();
    
        // Process all nodes
        var actionNodes = $(this.ACTION_NODE_CLASS);
        squelch(()=>{
            actionNodes.each((index, element)=>{
                try {
                    console.log(`Processing node ${index}`);
                    formatter.processActionNode(element);
                } catch(err) {
                    console.error(`Error while processing node ${index}:\n${err}`);
                }
            });
        });
    
        console.log(`Finished processing ${actionNodes.length} actions`);
    }

}

// end of artdungeon/SlowArdScheduler.js

// =========================================
//  artdungeon/utilities.js
// =========================================

// +----------------------------------+
// | Misc functions from the internet |
// +----------------------------------+
/**
 * Silences console.log within a certain code block
 */
function squelch(code) {
    var log = console.log;
    console.log = ()=>{};
    var retval = code();
    console.log = log;
    return retval;
}

function replaceAt(str, idx, target, replacement) {
    return str.slice(0,idx).concat(replacement, str.slice(idx + target.length));
}


// end of artdungeon/utilities.js

// =========================================
//  artdungeon/Run.last.js
// =========================================
var scheduler = USER_CONFIG.slowScheduler ? new SlowArdScheduler() : new FastArdScheduler();

scheduler.start();

// end of artdungeon/Run.last.js

