proc compileFile {filePath targetChannel} {
    try {
        set fileChan [open $filePath r]
        set fileContents [read $fileChan]
        
        # Add filename to each concatenated file
        puts $targetChannel "// ========================================="
        puts $targetChannel "//  $filePath"
        puts $targetChannel "// ========================================="
        puts $targetChannel $fileContents
        puts $targetChannel "\n// end of $filePath\n"

        puts "Added $filePath succesfully"

    } on error err {
        set errmsg "Failed to add $filePath:\n $err"

    } finally {
        if {[info exists fileChan]} {close $fileChan}
    }

    if {[info exists errmsg]} {error $errmsg}
}

proc compileDirectory {dir destfile} {
    if {![file isdirectory $dir]} {
        error "$dir is not a directory"
    }

    set jsFiles [glob $dir/*.js]

    # Compile '.first.js' first
    set firstIdx [lsearch $jsFiles $dir/*.first.js]
    if {$firstIdx != -1} {
        set fname   [lindex $jsFiles $firstIdx]
        set jsFiles [lreplace $jsFiles $firstIdx $firstIdx]
        set jsFiles [linsert $jsFiles 0 $fname]
    }

    # Compile '.last.js' last
    set lastIdx [lsearch $jsFiles $dir/*.last.js]
    if {$lastIdx != -1} {
        set fname   [lindex $jsFiles $lastIdx]
        set jsFiles [lreplace $jsFiles $lastIdx $lastIdx]
        set jsFiles [linsert $jsFiles end $fname]
    }

    # Delete old compiled file before compiling
    file delete $destfile

    try {
        set fileChan [open $destfile w+]
        puts $fileChan "// ========================================="
        puts $fileChan "//  NOTE FOR DEVELOPERS:"
        puts $fileChan "//  This file was generated from the"
        puts $fileChan "//  folder $dir."
        puts $fileChan "//  If you intend to push changes, do not"
        puts $fileChan "//  modify this file directly or they will"
        puts $fileChan "//  be lost on recompile."

        foreach jsFile $jsFiles {compileFile $jsFile $fileChan}

        puts "Successfully compiled $destfile"

    } on error err {
        set errmsg "Failed to compile $destfile: $err"

    } finally {
        if {[info exists fileChan]} {close $fileChan}
    }

    if {[info exists errmsg]} {error $errmsg}
}