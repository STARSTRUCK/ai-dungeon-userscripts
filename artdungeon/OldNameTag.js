/**
 * Defines location and text of an already-processed name.
 */
class OldNameTag {
    constructor(formatter) {
        this.REGEXP_OLD_NAME_TAG = /\[OLD_NAME_TAG\]/m;

        // Properties
        this.idx    = Infinity;
        this.exists = false;
        this.text   = ""
        this.formatter = formatter;
    }

    from(html) {
        var idx = html.search(this.REGEXP_OLD_NAME_TAG);

        if (idx == -1) {
            this.idx = Infinity;
            this.exists = false;
            return;

        } else {
            this.idx = idx;
            this.exists = true;
            this.text = '\n';
        }

        return this;
    }

    process(html) {
        const idx = this.idx;
        const text = this.text;

        console.log(`Processing old name tag at ${idx}`);

        this.formatter.reportPortrait();

        return { idx: idx + this.text.length, html: html };
    }

    ignore(html) {
        return { idx: this.idx + this.text.length, html: html };
    }
}
