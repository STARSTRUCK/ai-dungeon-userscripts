/**
 * The AI Dungeon component of Art Dungeon.
 * Adds images to the adventure.
 */
class FastArdScheduler {
    constructor() {
        // Constants
        this.VERSION            = "0.2.0";
        this.MIN_LOOP_DELAY     = 200;
        this.ACTION_OUTER_CLASS = '#root';
        this.ACTION_NODE_CLASS  = '.r-1xnzce8';

        // Members
        this.actionObserver = null;
        this.loopReady      = false;
        this.dirtyPage      = false;
    }

    start() {
        console.log(`ART DUNGEON ${this.VERSION}: AI Dungeon component`);

        $(document).ready(this._startLoop.bind(this));
    }

    _startLoop() {
        this.loopReady = false;

        this._processAdventure();
    
        this._waitForDomChanges();
    
        setTimeout((()=>{
            if (this.dirtyPage) {
                this.dirtyPage = false;
                this._startLoop();
            } else {
                this.loopReady = true;
            }
        }).bind(this), this.MIN_LOOP_DELAY);
    }

    _processAdventure() {
        console.log(`Processing adventure`);

        // Start fresh each time the adventure is processed
        var formatter = new AdventureFormatter();
    
        // Process all nodes
        var actionNodes = $(this.ACTION_NODE_CLASS);
        squelch(()=>{
            actionNodes.each((index, element)=>{
                try {
                    console.log(`Processing node ${index}`);
                    formatter.processActionNode(element);
                } catch(err) {
                    console.error(`Error while processing node ${index}:\n${err}`);
                }
            });
        });
    
        console.log(`Finished processing ${actionNodes.length} actions`);
    }

    _waitForDomChanges() {
        this.actionObserver = new MutationObserver(this._handleDomChange.bind(this));
    
        var actionParents = $(this.ACTION_OUTER_CLASS);
        actionParents.each ((index, element) => {
            this.actionObserver.observe(element, {
                childList: true,
                characterData: true,
                attributes: true,
                subtree: true});
            });
    
        console.log(`Observing ${actionParents.length} nodes`);
    }

    _handleDomChange(mutationRecord = null) {
        // We only need to detect this once
        this.actionObserver.disconnect();
    
        if (this.loopReady) {
            console.log("_handleDomChange triggered with loop ready--reprocessing");
            this._startLoop();
        } else {
            console.log("_handleDomChange triggered with loop not ready-will reprocess");
            this.dirtyPage = true;
        }
    }
}