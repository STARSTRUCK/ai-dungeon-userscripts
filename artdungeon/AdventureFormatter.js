/**
 * 
 */
class AdventureFormatter {
    constructor() {
        // Constants
        this.WATCHDOG_TIMER = 500;
        
        // Properties
        this.nameTags = {};
        this.newlineCount = 0;
        this.maxNewlines = Infinity;
    }

    /**
     * Processes an action node.
     * @param   {string} element - Node to process
     */
    processActionNode(element) {
        console.log(`NewlineCount = ${this.newlineCount}`);

        // Mine text properties while we're at it.
        Components.textColor  = $(element).css("color");
        Components.textHeight = $(element).css("line-height").slice(0, -2);

        // Get HTML for THIS action.
        var html_done = "";
        var html_left = $(element).html();
        console.log(`Original action.html():\n${html_left}`);

        // Do this until we can't find any more names or ard tags.
        for (var loop = 0; loop <= this.WATCHDOG_TIMER; loop++) {

            console.log(`NewlineCount = ${this.newlineCount}`);
            console.log(`MaxNewlines = ${this.maxNewlines}`);

            // Get first point of interest but don't process it yet.
            const poi = this._getFirstPointOfInterestFrom(html_left);

            // If we can't find any more points of interest, abort.
            if (poi == null) {
                console.log("No more points of interest");
                break;
            }

            try {
                var res = poi.process(html_left);

            } catch(err) {
                console.error(`Failed to process point of interest ${poi.text}
                            at ${poi.idx}:\n${err}`);

                var res = poi.ignore(html_left);
            }

            console.log(`Results:`);
            console.log(`res.idx:${res.idx}`);
            console.log(`res.hmtl:\n${res.html}`);
            console.log(`html_done:\n${html_done}`);
            console.log(`html_left:\n${html_left}`);

            html_left = res.html;

            // Split string into processed and pending parts
            html_done = html_done.concat(html_left.slice(0,res.idx));
            html_left = html_left.slice(res.idx);

            if (loop >= this.WATCHDOG_TIMER) {
                console.error(
                    `WDT triggered while processing a node;
                    either ${this.WATCHDOG_TIMER} tags parsed in one node or something went wrong!`);
                break;
            }
        }

        // No more points of interest; concat remainder.
        html_done = html_done.concat(html_left);

        // Finally, commit changes to action span
        $(element).html(html_done);
        console.log(`Committed changes to node:\n${html_done}`);
    }
    
    _getFirstPointOfInterestFrom(html) {
        var pois = new Array();
    
        pois.push(new RawArdTag(this).from(html));
        pois.push(new NameTag(this).from(html));
        pois.push(new OldNameTag(this).from(html));
        pois.push(new OldEmbed(this).from(html));

        if (USER_CONFIG.floatLeft && this.maxNewlines != Infinity) {
            pois.push(new Newline(this).from(html));

            if (this.newlineCount > 0) {
                pois.push(new NotNewline(this).from(html));
            }
        }

        pois.sort((a,b)=>a.idx - b.idx);

        if (!pois[0].exists) {
            return null;
        } else {
            return pois[0];
        }
    }

    resetNewline() {
        this.newlineCount = 0;
    }

    reportPortrait() {
        this.newlineCount = 0;
        this.maxNewlines = 2;
    }

    reportNormalImage() {
        this.newlineCount = 0;
        this.maxNewlines = 3;
    }

    reportNewline() {
        this.newlineCount++;
    }

    reportFloatBreak() {
        this.newlineCount = 0;
        this.maxNewlines = Infinity;
    }

    shouldStopFloating() {
        return this.newlineCount >= this.maxNewlines;
    }
}