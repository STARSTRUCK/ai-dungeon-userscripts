
// +----------------------------------+
// | Misc functions from the internet |
// +----------------------------------+
/**
 * Silences console.log within a certain code block
 */
function squelch(code) {
    var log = console.log;
    console.log = ()=>{};
    var retval = code();
    console.log = log;
    return retval;
}

function replaceAt(str, idx, target, replacement) {
    return str.slice(0,idx).concat(replacement, str.slice(idx + target.length));
}
