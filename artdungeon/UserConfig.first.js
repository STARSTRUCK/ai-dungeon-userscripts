// ==UserScript==
// @name         Art Dungeon
// @namespace    https://gitlab.com/STARSTRUCK/ai-dungeon-userscripts
// @version      0.2.0
// @description  Embed Artbreeder images into AI Dungeon adventures and scenarios
// @author       STARSTRUCK / @corolla_johnson
// @match        https://play.aidungeon.io/*
// @require      http://code.jquery.com/jquery-latest.js
// ==/UserScript==

// +---------------------------------------+
// | User Config - Change these as needed! |
// +---------------------------------------+
const USER_CONFIG = {
    // If true, will wait for text to finish changing
    // before adding images. Reduces jerking/flashing.
    slowScheduler: false,

    // Whether to use wide name tags.
    wideNameTags: true,
    
    // Whether images should appear to the left of text.
    // Floating has certain rules that are written below.
    floatLeft: true,

    // The height of small images in line-heights.
    smallImageScale: 5,

    // The height of large images in line-heights.
    largeImageScale: 8,

    // Fullsize images are always 512px tall!

};

/**
 * Examples of Art Dungeon tags:
 *
 * [ard -id a161fcf77098472ed213]
 * - Tag will be replaced by the specified image followed by a newline.
 *
 * [ard -id a161fcf77098472ed213 -scale small -names "John Doe" "Johnny"]
 * - Associates the specified image with the names "John Doe" and "Johnny" (case insensitive)
 *   followed by a colon (i.e. "JOHN DOE:")
 * - Sets a small scale.
 * - Acts on all text BELOW the tag. The association can be overwritten if the same name(s)
 *   are used with a different image later.
 *
 * Full arg list:
 * -id:             The image ID. Required.
 *
 * -scale:          small - (Default) A small size, suitable for automatic dialog portraits
 *                  large - A larger size, suitable for manually-embedded items.
 *                  fullsize  - A very large size suitable for landscapes.
 *                  Exact scales are user-configurable.
 *                  Scales the image. The Artbreeder component will suggest a scale automatically.
 *                  Image will be downloaded at full scale regardless of what you choose.
 *
 * -names:          Must be the last arg. A list of names to associate with the image.
 *                  If not present, the tag will embed the specified image.
 *                  If a tag with -names is successfully parsed, Art Dungeon will replace the tag
 *                  with a message indicating the success.
 * 
 */

/**
 * Floating behaviour:
 *
 * - If USER_CONFIG.floatLeft is true, text will appear to the right of images, saving vertical
 *   space and generally looking quite nice.
 * 
 * - Text will STOP floating after a certain number of blank lines.
 * 
 * - For character portraits, one blank line is enough, e.g.
 *   +---------------------------------------------------------------------+
 *   | JOHN: I've made a huge mess.                                        |
 *   |                                                                     |
 *   | John recalled the series of mistakes that led him to this juncture. |
 *   +---------------------------------------------------------------------+
 *   This lets you intuitively separate dialog from prose.
 * 
 * - For regular images, two blank lines are needed.
 *   +---------------------------------------------------------------------+
 *   | [ard -id a161fcf77098472ed213]                                      |
 *   | John recalled the series of mistakes that led him to this juncture. |
 *   |                                                                     |
 *   |                                                                     |
 *   | In an different town, Jane was gloating about her good luck.        |
 *   +---------------------------------------------------------------------+
 *   Several paragraphs separated by a single blank line wiil float next 
 *   to the same image, but you can still break the floating with two blank lines.
 * 
 */