/**
 * Defines location and text of a recognized name.
 */
class NameTag {
    constructor(formatter) {
        // Properties
        this.idx    = Infinity;
        this.exists = false;
        this.text   = ""
        this.formatter = formatter;
    }

    from(html) {
        var firstIdx = Infinity;
        var firstName = null;

        for (const name in this.formatter.nameTags) {
            const regexp_speechTag = new RegExp(`${name}: `, 'i');

            const idx = html.search(regexp_speechTag);

            if (idx == -1) {
                continue;
            }

            if (idx < firstIdx) {
                firstIdx = idx;
                firstName = name;
            }
        }

        if (firstIdx == Infinity) {
            this.idx = Infinity;
            this.exists = false;
            return;

        } else {
            this.idx = firstIdx;
            this.name = firstName;
            this.exists = true;
        }

        console.log(`Final index for ${this.name} = ${this.idx}`);

        return this;
    }

    process(html) {
        const idx = this.idx;
        const name = this.name;

        console.log(`Adding character image for ${name} at ${idx}`);

        var portrait = Components.Portrait(this.formatter.nameTags[name], name);

        html = replaceAt(html, idx, name.concat(": "), portrait);
        console.log(`New HTML:\n${html}`);

        this.formatter.reportPortrait();
            
        var lastIdx = idx + portrait.length;
        
        return { idx: lastIdx, html: html };
    }

    ignore(html) {
        return { idx: this.idx + this.text.length, html: html };
    }
}