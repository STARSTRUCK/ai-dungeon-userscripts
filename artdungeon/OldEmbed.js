/**
 * Defines location and text of an already processed standard image.
 */
class OldEmbed {
    constructor(formatter) {
        this.REGEX_OLD_EMBED = /\[OLD_EMBED\]/m;

        // Properties
        this.idx    = Infinity;
        this.exists = false;
        this.text   = ""
        this.formatter = formatter;
    }

    from(html) {
        var idx = html.search(this.REGEX_OLD_EMBED);

        if (idx == -1) {
            this.idx = Infinity;
            this.exists = false;
            return;

        } else {
            this.idx = idx;
            this.exists = true;
            this.text = '\n';
        }

        return this;
    }

    process(html) {
        const idx = this.idx;
        const text = this.text;

        console.log(`Processing old embed at ${idx}`);

        this.formatter.reportNormalImage();

        return { idx: idx + this.text.length, html: html };
    }

    ignore(html) {
        return { idx: this.idx + this.text.length, html: html };
    }
}
