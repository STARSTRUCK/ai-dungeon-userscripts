/**
 * Defines location and text of a raw [ard] tag.
 */
class RawArdTag {
    constructor(formatter) {
        // Constants
        this.REGEXP_ARDTAG = /(\[ard)(\w|\d|-| |")*(\])/m;

        // Properties
        this.idx    = Infinity;
        this.exists = false;
        this.text   = ""
        this.formatter = formatter;
    }

    from(html) {
        var idx = html.search(this.REGEXP_ARDTAG);

        if (idx == -1) {
            return this;

        } else {
            this.idx    = idx;
            this.exists = true;
            this.text   = (html.match(this.REGEXP_ARDTAG))[0];
        }
        
        return this;
    }

    process(html) {
        const text = this.text;
        const idx = this.idx;

        try {
            var parsedTag = new ArdTag(this.text);
            console.log(`Successfully parsed tag: ${parsedTag} at ${idx}`);

        } catch (err) {
            console.error(`Ignoring bad tag: ${err} at ${idx}`);
            return { idx: idx + text.length, html: html };
        }

        if (parsedTag.names.length == 0) {
            console.log(`No names found in tag; embedding in place`);

            var image = Components.NamelessImage(parsedTag);

            html = replaceAt(html, idx, text, image);
            var lastIdx = idx + image.length;

            if (USER_CONFIG.floatLeft) {
                // Destroy newline after image if it is present.
                if (html.charAt(lastIdx) == '\n') {
                    console.log("Eating newline");
                    html = replaceAt(html, lastIdx, '\n', '');
                }
            }

            this.formatter.reportNormalImage();

            console.log(`New HTML:\n${html}`);

            return { idx: lastIdx, html: html };

        } else {
            // Generate (or overwrite) a name-tag association.
            for (const name of parsedTag.names) {
                console.log(`New association: ${name}-${parsedTag.id}`);
                this.formatter.nameTags[name] = parsedTag;
            }

            if (parsedTag.indicate) {
                // Add -noind flag to tag (re-link but don't re-indicate)
                const text_noind = "[ard -noind".concat(text.slice(4));

                // Indicate succesful parsing
                // (Leave raw tag for re-linking)
                const indicatedTag = `<nobr><span style="background-color:${Components.textColor};color:black;">
                                &check; Image ${parsedTag.id} linked for names: [${parsedTag.names}]
                                </span></nobr><span style="display:none;">${text_noind}</span>`;

                html = replaceAt(html, idx, text, indicatedTag);
                console.log(`New HTML:\n${html}`);

                this.formatter.resetNewline();

                return { idx: idx + indicatedTag.length, html: html };

            } else {

                this.formatter.resetNewline();

                return { idx: idx + text.length, html: html };
            }
        }
    }

    ignore(html) {
        return { idx: this.idx + this.text.length, html: html };
    }
}