/**
 * Parsed Art Dungeon tag
 * @param   {string} tag    - The text of the tag.
 * @returns {ArdTag}        - Parsed ardtag object.
 */
class ArdTag {
    constructor(tag) {
        // +-----------+
        // | Constants |
        // +-----------+
        // Matches the spaces used as delimiters within the tags
        // (unless in quotes; this allows you to specify character names with spaces in them.)
        this.REGEXP_SPLIT_BY_SPACE_EXCEPT_IN_QUOTES = /(?:[^\s"]+|"[^"]*")+/g;

        // Used for input validation.
        // Matches anything other than alphanumeric, underscores, spaces and dashes.
        this.REGEXP_INPUT_SANITY = /[^a-zA-Z0-9_ \-]/m;

        // TT for state_getNextOption
        this.GNO_TRANSITION_TABLE = {
            "ard"   : this._state_getNextOption,
            "-id"   : this._state_getIdArg,
            "-scale": this._state_getScaleArg,
            "-names": this._state_getNamesArg,
            "-noind": this._state_noind,
        };

        // +-----------+
        // | Variables |
        // +-----------+
        this.id = -1;
        this.scale = "small";
        this.names = new Array();
        this.indicate = true;

        console.log(`Parsing tag: ${tag}`);

        // Strip square brackets
        tag = tag.slice(1, -1);

        // Split by spaces
        var tokens = tag.match(this.REGEXP_SPLIT_BY_SPACE_EXCEPT_IN_QUOTES);
        console.log(`Got tokens ${tokens}`);

        // Parse with FSM
        var state = this._state_getNextOption.bind(this);
        for (var currentToken of tokens) {
            
            // Strip quotes from quoted strings
            currentToken = currentToken.replace('\"', "");
            currentToken = currentToken.replace('\"', "");

            // Check for possible HTML injection
            if (currentToken.search(this.REGEXP_INPUT_SANITY) != -1) {
                throw `Illegal characters found in token ${currentToken}`;
            }

            state = state(currentToken).bind(this);
        }

        if (this.id == -1) {
            throw "Tag has no image id";
        }
    }
    
    // +------------+
    // | FSM states |
    // +------------+
    _state_getNextOption(token) {
        console.log(`_state_getNextOption(${token})`);

        var next = this.GNO_TRANSITION_TABLE[token];
        if (next == undefined) {
            throw `Invalid option: ${token}; expected -id, -scale, -noind or -names`;
        }

        return next.bind(this);
    }

    _state_noind(token) {
        console.log(`_state_noind(${token})`);

        this.indicate = false;

        return this._state_getNextOption(token).bind(this);
    }

    _state_getIdArg(token) {
        console.log(`_state_getIdArg(${token})`);

        if (this.GNO_TRANSITION_TABLE[token] != undefined || token[0] == '-') {
            throw `Unexpected option: ${token}; expected image id`;
        }

        this.id = token;
        return this._state_getNextOption.bind(this);
    }

    _state_getScaleArg(token) {
        console.log(`_state_getScaleArg(${token})`);

        if (this.GNO_TRANSITION_TABLE[token] != undefined || token[0] == '-') {
            throw `Unexpected option: ${token}; expected scale`;
        }

        if (Components.imageHeights[token] == undefined) {
            throw `Unknown scale: ${token}; expected small, large or fullsize `;
        }

        this.scale = token;
        return this._state_getNextOption.bind(this);
    }

    _state_getNamesArg(token) {
        console.log(`_state_getNamesArg(${token})`);

        if (this.GNO_TRANSITION_TABLE[token] != undefined || token[0] == '-') {
            throw `Unexpected option: ${token}; expected name`;
        }

        this.names.push(token);
        return this._state_getNamesArg.bind(this);
    }
}
