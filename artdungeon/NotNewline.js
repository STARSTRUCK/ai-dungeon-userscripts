/**
 * Defines location and text of a character other than newline.
 */
class NotNewline {
    constructor(formatter) {
        this.REGEXP_NOT_NEWLINE = /[^\n]/m;

        // Properties
        this.idx    = Infinity;
        this.exists = false;
        this.text   = ""
        this.formatter = formatter;
    }

    from(html) {
        var idx = html.search(this.REGEXP_NOT_NEWLINE);

        if (idx == -1) {
            this.idx = Infinity;
            this.exists = false;
            return;

        } else {
            this.idx = idx;
            this.exists = true;
            this.text = html.charAt(idx);
        }

        return this;
    }

    process(html) {
        const idx = this.idx;
        const text = this.text;

        console.log(`Processing non-newline at ${idx}: ${text}`);

        this.formatter.resetNewline();

        return { idx: idx, html: html };
    }

    ignore(html) {
        return { idx: idx, html: html };
    }
}
