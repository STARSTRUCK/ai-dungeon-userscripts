class Components
{
    // Text height in px.
    static textHeight = 10;

    // Text color as rgb(r,g,b).
    static textColor = 'rgb(200, 200, 200)';

    // Scale settings to be set by user.
    static imageHeights = {
        small     : () => {return this.textHeight * USER_CONFIG.smallImageScale;},
        large     : () => {return this.textHeight * USER_CONFIG.largeImageScale;},
        fullsize  : () => {return 512},
    };
    
    static Portrait(ardtag, name) {
        // Add a linebreak to space the portrait from previous text.
        var tag = `<div style="clear:left"><br></div><span style="display:none;">[OLD_NAME_TAG]</span>`;

        if (USER_CONFIG.wideNameTags) {
            tag = tag.concat(this.ImageTag(ardtag, true));

            // Write the name in inverted text and put some color around it.
            tag = tag.concat(`<div style="background-color:${this.textColor};color:Black;`);
            tag = tag.concat(`padding:${this.textHeight/4}px ${this.textColor};">`);

            // Put a space if wide name tags are used without floatLeft
            if (!USER_CONFIG.floatLeft) {
                tag = tag.concat(' ');
            }

            tag = tag.concat(name);
            tag = tag.concat(`</div>`);

        } else {
            tag = tag.concat(this.ImageTag(ardtag, true));

            // Need a linebreak on non float-left configs
            if (!USER_CONFIG.floatLeft) {
                tag = tag.concat('<br>');
            }

            // Write the name in inverted text and put some color around it.
            tag = tag.concat(`<span style="background-color:${this.textColor};color:Black;`);
            tag = tag.concat(`padding:${this.textHeight/4}px;">`);
            tag = tag.concat(name);
            tag = tag.concat(`</span>`);

            // Need a line break if using floatLeft; else we need an enforced space
            if (USER_CONFIG.floatLeft) {
                tag = tag.concat('<br>');
            } else {
                tag = tag.concat(' ');
            }
        }

        return tag;
    }

    static NamelessImage(ardtag) {
        // Add a linebreak to space the portrait from previous text.
        var tag = `<div style="clear:left"><br></div><span style="display:none;">[OLD_EMBED]</span>`;

        tag = tag.concat(this.ImageTag(ardtag));

        // Need at least one line break if not using float left.
        if (!USER_CONFIG.floatLeft) {
            tag = tag.concat('<br>');
        }

        return tag;
    }

    /**
     * Returns a plain HTML image tag from an ArdTag.
     * @param   {ArdTag}  ardtag   - Parsed ardtag object.
     * @returns {string}           - HTML image tag.
     */
    static ImageTag(ardtag, border = false) {
        
        var floatLeft = (ardtag.scale != this.imageHeights.fullsize && USER_CONFIG.floatLeft);

        var src =     `src="https://artbreeder.b-cdn.net/imgs/${ardtag.id}.jpeg"`
        var altText = `alt="Art Dungeon v0.1 - id:${ardtag.id}, scale:${ardtag.scale}, names:${ardtag.names}"`
        var height =  `height=${(this.imageHeights[ardtag.scale])()}px`;

        var style_start  = `style="`;
        var style_float  = floatLeft ? `float:left;clear:left;` : ``;
        var style_border = border ? `border:${this.textHeight/8}px solid ${this.textColor};` : ``;
        var style_margin = `margin-right:${this.textHeight/2}px;`
        var style_end    = `"`;

        var tag =        `<img ${src} ${altText} ${height} ${style_start} ${style_float}`
        tag = tag.concat(`${style_margin} ${style_border} ${style_end}>`)

        return tag;
    }
}
