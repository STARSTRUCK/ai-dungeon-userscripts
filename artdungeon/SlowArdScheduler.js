/**
 * The AI Dungeon component of Art Dungeon.
 * This class controls the scheduling of AdventureFormatter.
 */
class SlowArdScheduler {
    constructor() {
        // Constants
        this.VERSION            = "0.2.0";
        this.STABILIZE_TIME     = 10;
        this.ACTION_OUTER_CLASS = '#root';
        this.ACTION_NODE_CLASS  = '.r-1xnzce8';

        // Members
        this.domObserver         = null;
        this.dirtyPage           = false;
        this.stabilizing         = false;
    }

    start() {
        console.log(`ART DUNGEON ${this.VERSION}: AI Dungeon component`);

        $(document).ready(this._run.bind(this));
    }

    _run() {
        if (this.domObserver != null) {
            this.domObserver.disconnect();
        }

        this._processAdventure();

        this._setupDomObserver();
    }

    _setupDomObserver() {
        this.domObserver = new MutationObserver(this._handleDomChange.bind(this));
    
        var actionParents = $(this.ACTION_OUTER_CLASS);
        actionParents.each ((index, element) => {
            this.domObserver.observe(element, {
                childList: true,
                characterData: true,
                attributes: true,
                subtree: true});
            });
    
        console.log(`Observing ${actionParents.length} nodes`);
    }

    _handleDomChange(mutationRecord = null) {
        this.dirtyPage = true;

        if (!this.stabilizing) {
            console.log("_handleDomChange triggered while not stabilizing");
            this._waitUntilDomStabilizes();
        }
    }

    _waitUntilDomStabilizes() {
        this.stabilizing = true;

        console.log("Waiting until dom stabilizes...")

        if (!this.dirtyPage) {
            this.dirtyPage = false;
            this.stabilizing = false;
            this._run();
            return;

        } else {
            console.log("Page was dirty")
            this.dirtyPage = false;
            setTimeout(this._waitUntilDomStabilizes.bind(this), 
                       this.STABILIZE_TIME);    
        }
    }

    _waitForDomChange() {
        console.log("waitingForDomChange");
        this.stabilizing = true;
    }

    
    _processAdventure() {
        console.log(`Processing adventure`);

        // Start fresh each time the adventure is processed
        var formatter = new AdventureFormatter();
    
        // Process all nodes
        var actionNodes = $(this.ACTION_NODE_CLASS);
        squelch(()=>{
            actionNodes.each((index, element)=>{
                try {
                    console.log(`Processing node ${index}`);
                    formatter.processActionNode(element);
                } catch(err) {
                    console.error(`Error while processing node ${index}:\n${err}`);
                }
            });
        });
    
        console.log(`Finished processing ${actionNodes.length} actions`);
    }

}