/**
 * Defines location and text of a newline.
 */
class Newline {
    constructor(formatter) {
        this.REGEXP_NEWLINE = /\n/m;

        // Properties
        this.idx    = Infinity;
        this.exists = false;
        this.text   = ""
        this.formatter = formatter;
    }

    from(html) {
        var idx = html.search(this.REGEXP_NEWLINE);

        if (idx == -1) {
            this.idx = Infinity;
            this.exists = false;
            return;

        } else {
            this.idx = idx;
            this.exists = true;
            this.text = '\n';
        }

        return this;
    }

    process(html) {
        const idx = this.idx;
        const text = this.text;

        console.log(`Processing linebreak at ${idx}`);

        this.formatter.reportNewline();

        if (this.formatter.shouldStopFloating()) {
            const specialdiv = `<div style="clear:left"><br></div>`;

            html = replaceAt(html, idx, '\n', specialdiv);

            console.log(`New HTML:\n${html}`);
    
            this.formatter.reportFloatBreak();

            return { idx: idx + specialdiv.length, html: html };
        } else {
            return this.ignore(html);
        }
    }

    ignore(html) {
        return { idx: this.idx + this.text.length, html: html };
    }
}
