# AI Dungeon Userscripts

A collection of Tampermonkey / Greasemonkey scripts for Latitude's AI Dungeon.

## How to install
1. Install the Tampermonkey extension on Chrome or Firefox.
2. Click on artdungeon.user.js in the file list.
3. Click 'Open Raw' on the right (the icon with a page that says '>_').
   (Alternatively, just click this: https://gitlab.com/STARSTRUCK/ai-dungeon-userscripts/-/raw/master/artdungeon.user.js)
4. Tampermonkey will prompt you to install the script. Click 'install'.
5. Reload AI Dungeon.

## Art Dungeon
Adds Artbreeder images to suitably-annotated AI Dungeon adventures.
- Manually embed pictures at suitable points in the adventure-- or...
- Link a portrait to a character's name to have it automatically appear when they speak.
- Re-link the portrait mid-adventure to have a character age, change clothing or expression.

## What's new in v0.2.0
- Added left-floating for images (images sit left of text, saving vertical space and generally looking quite baller).
- Added a slow scheduler if you find the normal update speed too jerky.
- Added a USER_CONFIG object to the beginning of the file, allowing you to turn off left-floating,
  turn on the slow scheduler and change the relative size of images.
- (Backend) Performed a full OO refactor, splitting the monolithic userscript into single-class files.
- (Backend) Wrote a concatenator in Tcl.

### Screenshots
![Screenshot](res/newimg_3.PNG)
![Screenshot](res/newimg_2.PNG)
![Screenshot](res/newimg_1.PNG)
![Screenshot](res/newimg_4.PNG)

### Test Adventure
https://play.aidungeon.io/main/adventureView?publicId=992098f1-7e12-4b7f-a020-e797490fb687

### Notation
```js
/**
 * Examples of Art Dungeon tags:
 *
 * [ard -id a161fcf77098472ed213]
 * - Tag will be replaced by the specified image followed by a newline.
 *
 * [ard -id a161fcf77098472ed213 -scale small -names "John Doe" "Johnny"]
 * - Associates the specified image with the names "John Doe" and "Johnny" (case insensitive)
 *   followed by a colon (i.e. "JOHN DOE:")
 * - Sets a small scale.
 * - Acts on all text BELOW the tag. The association can be overwritten if the same name(s)
 *   are used with a different image later.
 *
 * Full arg list:
 * -id:             The image ID. Required.
 *
 * -scale:          small - (Default) A small size, suitable for automatic dialog portraits
 *                  large - A larger size, suitable for manually-embedded items.
 *                  fullsize  - A very large size suitable for landscapes.
 *                  Exact scales are user-configurable.
 *                  Scales the image. The Artbreeder component will suggest a scale automatically.
 *                  Image will be downloaded at full scale regardless of what you choose.
 *
 * -names:          Must be the last arg. A list of names to associate with the image.
 *                  If not present, the tag will embed the specified image.
 *                  If a tag with -names is successfully parsed, Art Dungeon will replace the tag
 *                  with a message indicating the success.
 */
```

### Formatting
```js
/**
 * Floating behaviour:
 *
 * - If USER_CONFIG.floatLeft is true, text will appear to the right of images, saving vertical
 *   space and generally looking quite nice.
 * 
 * - Text will STOP floating after a certain number of blank lines.
 * 
 * - For character portraits, one blank line is enough, e.g.
 *   +---------------------------------------------------------------------+
 *   | JOHN: I've made a huge mess.                                        |
 *   |                                                                     |
 *   | John recalled the series of mistakes that led him to this juncture. |
 *   +---------------------------------------------------------------------+
 *   This lets you intuitively separate dialog from prose.
 * 
 * - For regular images, two blank lines are needed.
 *   +---------------------------------------------------------------------+
 *   | [ard -id a161fcf77098472ed213]                                      |
 *   | John recalled the series of mistakes that led him to this juncture. |
 *   |                                                                     |
 *   |                                                                     |
 *   | In an different town, Jane was gloating about her good luck.        |
 *   +---------------------------------------------------------------------+
 *   Several paragraphs separated by a single blank line wiil float next 
 *   to the same image, but you can still break the floating with two blank lines.
 * 
 */
```

### Known Issues
- Minor inconsistencies in floating behaviour between adventureView and adventurePlay.
- Inline editing flings the scrollbar around.